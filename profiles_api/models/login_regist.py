from django.db import models
from django.contrib.auth.models import AbstractBaseUser, UserManager
from django.contrib.auth.models import PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password
from datetime import datetime
from django.utils.timezone import now

class UserProfileManager(BaseUserManager):

    def create_user(self, email, password, fullName, telep):
    # Crear usuario de acuerdo a la base 
        if not email and not password and not fullName and not telep:
            return ValueError('Datos faltantes o inválido para el registro')

        userDef = self.model(email = email, fullName=fullName, telep=telep)
        userDef.set_password(password)
        userDef.save(using=self._db)

        return userDef
    def create_superuser(self, email, fullName, password, telep):
        superUser = self.create_user(email, password, fullName, telep)
        superUser.is_superuser = True
        superUser.is_staff = True
        superUser.save(using=self._db)
        return superUser

class BaseUserProfile(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    email = models.EmailField('Correo electrónico',max_length=200, unique=True)
    telep = models.CharField('Teléfono',max_length=20)
    password = models.CharField('Password', max_length = 256)
    fullName = models.CharField('Nombre y apellido',max_length=200)
    dateOfRegis = models.DateField('Fecha de registro',default=datetime.now)
    addressHouse = models.CharField('Dirección',max_length=300, blank=True)
    cityAct = models.CharField('Ciudad de residencia',max_length=300, blank=True)
    yearOld = models.IntegerField('Edad', default=18)
    last_login = models.DateField(default=now)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    def save(self, **kwargs):
        self.password = make_password(self.password, 
        'mMUj0DrIK6vgtdIYepkIxN')
        super().save(**kwargs)

    objects = UserProfileManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['fullName','telep']
    