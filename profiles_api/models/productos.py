from django.db import models

class Productos(models.Model):

    ID_Prod = models.BigAutoField(primary_key=True)
    nombre_Prod = models.CharField(max_length=200, blank=False, unique=True)
    precio_Prod = models.FloatField()
    numStock_Prod = models.IntegerField()
