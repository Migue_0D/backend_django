from django.db import models
from django.utils.timezone import now
from .login_regist import BaseUserProfile
from .productos import Productos

estado = [

    (1,'En proceso'),
    (2,'Terminado')

]

class Pedidos(models.Model):

    ID_pedid = models.AutoField(primary_key=True)
    ID_prod = models.ForeignKey(Productos, related_name='Reserva_Productos', on_delete=models.CASCADE, default=0)
    ID_Cli = models.ForeignKey(BaseUserProfile, related_name='Pedido_Cliente', on_delete=models.CASCADE, default=0)
    fecha_pedid = models.DateTimeField(default=now, blank=True)
    estado_pedid = models.IntegerField(default=1,choices=estado)