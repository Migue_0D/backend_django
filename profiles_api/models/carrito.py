from django.db import models
from .productos import Productos

class Carrito(models.Model):
    ID_Carrito = models.BigAutoField(primary_key=True)
    ID_Prod = models.ForeignKey(Productos, related_name='Productos_Carrito',on_delete=models.CASCADE)
    cantidadPro_Carrito = models.IntegerField()

