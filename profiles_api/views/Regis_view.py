from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from profiles_api.serializers.regisUser_serializer import LoginResgistro_serializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class RegisterView(APIView):

    def post(self, request,*args,**kwargs):
        serializer = LoginResgistro_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        token_data = {"email":request.data['email'],
        "password":request.data["password"]}

        try:
            tokenSerializer = TokenObtainPairSerializer(data=token_data)
            tokenSerializer.is_valid(raise_exception=True)
            return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)
        except Exception as e:
            print(e)
            return Response('Error in token generation', status=status.HTTP_500_INTERNAL_SERVER_ERROR)
