from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from profiles_api.serializers.productos_serializer import productosSerializer
from profiles_api.models.productos import Productos
from rest_framework_simplejwt.backends import TokenBackend
from django.conf import settings
from rest_framework.permissions import IsAuthenticated

class Productos_view(APIView):

    def get(self, request):
        queryset = Productos.objects.all()
        serializer_producto = productosSerializer(queryset, many=True)
        return Response(serializer_producto.data)

    

    def post(self, request,*args, **kwargs):
        serializer_class   = productosSerializer
        permission_classes = (IsAuthenticated,)
        token        = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data   = tokenBackend.decode(token,verify=False)
        
        serializer_Producto = productosSerializer(data=request.data)
        serializer_Producto.is_valid(raise_exception=True)
        serializer_Producto.save()

        return Response(data=serializer_Producto.to_representation(request.data),status=status.HTTP_201_CREATED)
        


        
