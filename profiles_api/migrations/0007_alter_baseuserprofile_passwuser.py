# Generated by Django 3.2.8 on 2021-10-26 23:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles_api', '0006_auto_20211026_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baseuserprofile',
            name='passwUser',
            field=models.CharField(max_length=300, verbose_name='Pass'),
        ),
    ]
