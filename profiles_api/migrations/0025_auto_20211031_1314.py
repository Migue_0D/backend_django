# Generated by Django 3.2.8 on 2021-10-31 18:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('profiles_api', '0024_auto_20211029_1334'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pedidos',
            name='ID_Carrito',
        ),
        migrations.AddField(
            model_name='pedidos',
            name='ID_prod',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, related_name='Reserva_Productos', to='profiles_api.productos'),
        ),
        migrations.AlterField(
            model_name='baseuserprofile',
            name='fullName',
            field=models.CharField(max_length=200, verbose_name='Nombre y apellido'),
        ),
        migrations.AlterField(
            model_name='pedidos',
            name='ID_Cli',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, related_name='Pedido_Cliente', to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='Carrito',
        ),
    ]
