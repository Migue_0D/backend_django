# Generated by Django 3.2.8 on 2021-10-29 05:36

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles_api', '0019_alter_baseuserprofile_last_login'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baseuserprofile',
            name='last_login',
            field=models.DateField(default=datetime.datetime.now),
        ),
    ]
