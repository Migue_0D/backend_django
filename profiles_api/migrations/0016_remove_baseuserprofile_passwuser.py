# Generated by Django 3.2.8 on 2021-10-29 05:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles_api', '0015_alter_baseuserprofile_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='baseuserprofile',
            name='passwUser',
        ),
    ]
