from rest_framework import serializers
from profiles_api.models.productos import Productos

class productosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Productos
        fields = ['nombre_Prod','precio_Prod','numStock_Prod']

        def to_representation(self,obj):
            product = Productos.objects.get(id=obj.ID_Prod) 
            return {
                'Código de producto': product.ID_Prod,
                'Nombre': product.nombre_Prod,
                'Precio': product.precio_Prod,
                'Cantidad de stock': product.numStock_Prod
            }