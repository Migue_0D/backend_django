from rest_framework import serializers
from profiles_api.models.login_regist import BaseUserProfile

class LoginResgistro_serializer(serializers.ModelSerializer):

    class Meta:
        model = BaseUserProfile
        fields = ['email','fullName','password','telep']

        def create(self,validated_data):
            processCreate = BaseUserProfile.objects.create_user(**validated_data)
            return processCreate

        def to_representation(self,obj):
            toSearchUser = BaseUserProfile.objects.get(id=obj.id)
            return {
                'id': toSearchUser.id,
                'tel_client': toSearchUser.telep,
                'direccion_client': toSearchUser.addressHouse,
                'nombre_apellido': toSearchUser.fullName,
                'email': toSearchUser.email,
                'fecha de registro:': toSearchUser.dateOfRegis,
                'is_active': toSearchUser.is_active
            }
        