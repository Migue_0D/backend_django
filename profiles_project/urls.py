from django.contrib import admin
from django.urls import path
from profiles_api.views.Regis_view import RegisterView
from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView
from profiles_api.views.productos_view import Productos_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/',RegisterView.as_view()),
    path('login/',TokenObtainPairView.as_view()),
    path('refresh/',TokenRefreshView.as_view()),
    path('products/',Productos_view.as_view()),
]
